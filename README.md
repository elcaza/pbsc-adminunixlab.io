# Universidad Nacional Autónoma de México
# Dirección General de Cómputo y Tecnologías de Información y Comunicación
# UNAM-CERT
# Plan de Becarios de Seguridad en Cómputo
# Administración y Seguridad en Linux y UNIX

## Proyecto final

Instalar una infraestructura distribuida en una nube pública que contemple distintos servicios de red en servidores GNU/Linux conectados a través de una VPN.

![Diagrama de conexión de servicios](img/diagrama.png "Diagrama de conexión de servicios")

### Condiciones

+ No cambiar la contraseña de `root`
+ No quitar las llaves existentes en `~/.ssh/authorized_keys`
  * Cambiar propietario y permisos para que funcione la autenticación SSH con llaves
    - `chown -R usuario:grupo ~/.ssh`
    - `chmod -c 0700 ~/.ssh`
    - `chmod -c 0600 ~/.ssh/authorized_keys*`
+ Usuarios y grupos
  * No quitar ni modificar usuarios existentes
  * Crear un usuario normal `becarios` y habilitar `sudo` sin contraseña con `NOPASSWD`
  * Crear un usuario normal para **cada persona del equipo** (ej. `andres-hernandez`)
    - Agregar su llave pública para autenticación SSH
    - Agregar a los usuarios a un _grupo privilegiado_ (`sudo` o `wheel`) para dar privilegios de `sudo`
+ Tener cuidado con las reglas de `iptables`
  * Utilizar `iptables-apply` para evitar _cerrar la puerta_
  * Considerar el uso de `iptables-persistent`, `ufw` o `firewalld`
+ Tener cuidado en la configuración del servicio `ssh`
+ Permitir **siempre** las conexiones de los siguientes equipos
  * `tonejito.cf`
  * `becarios.tonejito.info`
  * `becarios.priv.tonejito.info`
  * Rango de VPN `10.0.8.0/24` (revisar esto con el equipo responsable del servicio)
  * Direcciones IP de las máquinas de los otros equipos
  * Rangos IP de RedUNAM `132.247.0.0/16` y `132.248.0.0/16`

#### Monitoreo

+ Los equipos serán monitoreados a través de **Nagios**
  * <https://becarios.tonejito.info/nagios3/?corewindow=/cgi-bin/nagios3/status.cgi?host=all>

### Equipos

#### Database

| Documentación | <https://gitlab.com/PBSC-AdminUNIX/2018/database#readme> |
|:-----:|:-----:|
| Host	| `database.becarios.tonejito.info` |
| IP	| `18.217.216.172` |
| OS	| CentOS 7 |

+ Instalar el servidor de **MySQL** v5.7 y **PostgreSQL v10**
  * Desde el repositorio oficial utilizando paquetes
    - <https://dev.mysql.com/downloads/repo/yum/>
    - <https://dev.mysql.com/doc/mysql-yum-repo-quick-guide/en/>
    - <https://yum.postgresql.org/>
    - <https://yum.postgresql.org/repopackages.php>
    - <https://wiki.postgresql.org/wiki/YUM_Installation>
  * Este equipo tendrá el servidor `master` de MySQL y tendrá que coordinarse con el equipo **storage** para que configuren la réplica
+ Instalar `phpMyAdmin` y `phpPgAdmin`
  * No importa si es desde `.tar.gz`, paquetes o con `git`
  * No importa si PHP se configura como módulo de Apache o como FPM
+ Instalar *OpenVPN* desde paquetes
+ Este equipo tendrá la CA subordinada que firmará los certificados de cliente para la VPN
+ Les ayudará bastante configurar el repositorio de *EPEL*
  * <https://fedoraproject.org/wiki/EPEL>
--------

#### Directory

| Documentación | <https://gitlab.com/PBSC-AdminUNIX/2018/directory#readme> |
|:-----:|:-----:|
| Host	|`directory.becarios.tonejito.info` |
| IP	|`18.224.160.100` |
| OS	|Debian 8 |

+ Instalar **OpenLDAP** desde paquetes
+ Instalar **LDAP Account Manager** y **LDAP Toolbox Self Service**
  * No importa si es desde `.tar.gz`, paquetes o con `git`
  * No importa si PHP se configura como módulo de Apache o como FPM

--------

#### Mail

| Documentación | <https://gitlab.com/PBSC-AdminUNIX/2018/mail#readme> |
|:-----:|:-----:|
| Host	| `mail.becarios.tonejito.info` |
| IP	| `18.220.184.122` |
| OS	| CentOS 6 |

+ Instalar **Postfix** y **Dovecot** desde paquetes
  * Deben autenticar a los usuarios desde **LDAP**
  * Los buzones de correo se deben guardar en el servidor **NFS**
    - El equipo **storage** debería exportar el directorio `/srv/home`
    - El equipo **directory** debería configurar el directorio `/srv/home/%u` como el *home* de cada usuario de **LDAP**
+ Abrir los puertos `25/tcp` (smtp), `465/tcp` (smtps) y `587/tcp` (submission)
+ Abrir el puerto `993/tcp` (imaps)
+ Permitir el relay desde las direcciones IP públicas y privadas de todos los demás equipos
+ Instalar **SquirrelMail** y configurar para enviar correo
  * No importa si es desde `.tar.gz`, paquetes o con `git`
  * No importa si PHP se configura como módulo de Apache o como FPM
* Les ayudará bastante configurar el repositorio **EPEL**
+ Debe configurarse la conexión a **LDAP** para que se reconozcan los usuarios

--------

#### Storage

| Documentación | <https://gitlab.com/PBSC-AdminUNIX/2018/storage#readme> |
|:-----:|:-----:|
| Host	| `storage.becarios.tonejito.info` |
| IP	| `18.224.238.25` |
| OS	| Ubuntu 18.04 LTS |

+ Instalar y configurar el servidor de **NFS** v4
  * Exportar el directorio `/srv/home` para el equipo **mail**
  * Exportar el directorio `/srv/www` para que el equipo **web** lo monte en `/var/www` y pueda guardar los _htdocs_ de `redmine` y `wordpress`
+ Usuarios de NFS
  * Debe configurarse la conexión a **LDAP** para que se guarden adecuadamente los buzones de correo
  * Configurar el propietario del directorio `/srv/www` como el usuario `www-data`
    - Verificar el usuario `www-data` tenga los mismos *uid* y *gid* en el equipo **web** y **storage**
    - Considerar el uso de las directivas `all_squash`, `anonuid` y `anongid`, ver [`man 5 exports`](https://linux.die.net/man/5/exports)
+ Instalar y configurar la replica de **MySQL** v5.7
  * Pueden utilizar el paquete que da el sistema operativo o el repositorio oficial de MySQL
  * Apoyarse en el equipo **database** para que les indiquen que hacer para empezar a replicar la base de datos
+ Instalar y configurar **BackupPC** desde paquetes
  * Utilizar `backuppc` para **copiar** los respaldos de otros equipos
    - Directorio LDAP (archivo ldif) generado por el equipo **directory**
    - Respaldos en formato SQL de MySQL y PostgreSQL generado por el equipo **database**

--------

#### Web

| Documentación | <https://gitlab.com/PBSC-AdminUNIX/2018/web#readme> |
|:-----:|:-----:|
| Host	| `web.becarios.tonejito.info` |
| IP	| `18.216.73.191` |
| OS	| Debian 9 |

+ Instalar **Apache 2.4** desde paquetes
+ Instalar **PHP 7.1 FPM** desde el repositorio **DotDeb** o **Sury**
  * <https://www.dotdeb.org/instructions/>
  * <https://deb.sury.org/>
  * Configurar el handler FastCGI de Apache
+ Instalar **WordPress**
  * No importa si es desde `.tar.gz`, paquetes o con `git`
  * WordPress debe conectarse a la base de datos *MySQL* en el equipo **database**
+ Instalar **Redmine** v3.4
  * No importa si se instala `ruby`, `rails` y `passenger` con paquetes o utilizando `rvm` y `gem install`
  * Redmine debe conectarse a la base de datos *PostgreSQL* en el equipo **database**
+ Instalar **RoundCube**
  * No importa si es desde `.tar.gz`, paquetes o con `git`
  * Configurarlo para que se conecte a Postfix y Dovecot del equipo **mail**

--------

